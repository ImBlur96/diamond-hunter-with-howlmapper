package com.howl.Main;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import com.neet.DiamondHunter.Entity.Diamond;
import com.neet.DiamondHunter.Entity.Item;
import com.neet.DiamondHunter.Entity.Player;
import com.neet.DiamondHunter.Manager.Locations;
import com.neet.DiamondHunter.TileMap.TileMap;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainWindowController implements Initializable {


    @FXML
    private BorderPane window;

    @FXML
    private Button updateAxe;

    @FXML
    private Button updateBox;

    @FXML
    private TextField axeX;

    @FXML
    private TextField boatX;

    @FXML
    private TextField axeY;

    @FXML
    private TextField boatY;

    @FXML
    private Canvas mapView;
    
    @FXML
    private MenuItem closeButton;
    
    private GraphicsContext gc;
    TileMap tilemap;
    ArrayList<Item> items;
	ArrayList<Diamond> diamonds;
	Player player;
	
	@FXML
	private void handleUpdateBoat(ActionEvent event) {
		try {
			Locations.boat_X = Integer.parseInt(boatX.getText())-1;
			Locations.boat_Y = Integer.parseInt(boatY.getText())-1;
			
			if(Locations.boat_X < 0 || Locations.boat_X > 39){
				throw new NumberFormatException();
			}
			else if(Locations.boat_Y < 0 || Locations.boat_Y > 39){
				throw new NumberFormatException();
			}
			
			Locations.write();
			
			drawMap(gc);
			
		} catch (NumberFormatException e) {
			System.err.println("Boat text fields are Invalid");
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("howlMapper Error");
			alert.setHeaderText("Coordinate field is invalid");
			alert.setContentText("X and Y fields must be filled with a positive integer (>0 and <=40)");
			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleUpdateAxe(ActionEvent event) {
		try {
			Locations.axe_X = Integer.parseInt(axeX.getText())-1;
			Locations.axe_Y = Integer.parseInt(axeY.getText())-1;
			
			if(Locations.axe_X < 0 || Locations.axe_X > 39){
				throw new NumberFormatException();
			}
			else if(Locations.axe_Y < 0 || Locations.axe_Y > 39){
				throw new NumberFormatException();
			}
			
			Locations.write();
			
			drawMap(gc);
			
		} catch (NumberFormatException e) {
			System.err.println("Axe text fields are Invalid");
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("howlMapper Error");
			alert.setHeaderText("Coordinate field is invalid");
			alert.setContentText("X and Y fields must be filled with a positive integer (>0 and <=40)");
			alert.showAndWait();
		}
	}
	
	@FXML
	private void closeButtonAction(){
	    Stage stage = (Stage) window.getScene().getWindow();
	    stage.close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		gc = mapView.getGraphicsContext2D();
        drawMap(gc);
		
	}
	
	private void fillInitialPositions(){
		
		axeX.setText(Integer.toString(Locations.axe_X+1));
		axeY.setText(Integer.toString(Locations.axe_Y+1));
		
		boatX.setText(Integer.toString(Locations.boat_X+1));
		boatY.setText(Integer.toString(Locations.boat_Y+1));
		
	}
	
	private void drawMap(GraphicsContext gc){ 
		
		Locations.getLocations();
    	
		fillInitialPositions();
		
    	diamonds = new ArrayList<Diamond>();
		items = new ArrayList<Item>();		
    	
    	tilemap = new TileMap(16);
		tilemap.loadTiles("/Tilesets/testtileset.gif");
		tilemap.loadMap("/Maps/testmap.map");
		tilemap.drawFullMap(gc);
		
		// draw player
		player = new Player(tilemap);
		player.setTilePosition(17, 17);
		player.drawFX(gc);
		
		// draw diamonds
		populateDiamonds();
		for(Diamond d : diamonds) {
			d.drawFX(gc);
		}
		
		// draw items
		populateItems();
		for(Item i : items) {
			i.drawFX(gc);
		}
		
    }
	
	private void populateDiamonds() {
	
		Diamond d;
		
		d = new Diamond(tilemap);
		d.setTilePosition(20, 20);
		d.addChange(new int[] { 23, 19, 1 });
		d.addChange(new int[] { 23, 20, 1 });
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(12, 36);
		d.addChange(new int[] { 31, 17, 1 });
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(28, 4);
		d.addChange(new int[] {27, 7, 1});
		d.addChange(new int[] {28, 7, 1});
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(4, 34);
		d.addChange(new int[] { 31, 21, 1 });
		diamonds.add(d);
		
		d = new Diamond(tilemap);
		d.setTilePosition(28, 19);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(35, 26);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(38, 36);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(27, 28);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(20, 30);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(14, 25);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(4, 21);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(9, 14);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(4, 3);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(20, 14);
		diamonds.add(d);
		d = new Diamond(tilemap);
		d.setTilePosition(13, 20);
		diamonds.add(d);
		
	}
	
	private void populateItems() {
		
		Item item;
		
		item = new Item(tilemap);
		item.setType(Item.AXE);
		item.setTilePosition(Locations.axe_X, Locations.axe_Y);
		items.add(item);
		
		item = new Item(tilemap);
		item.setType(Item.BOAT);
		item.setTilePosition(Locations.boat_X, Locations.boat_Y);
		items.add(item);
		
	}
}
