# README #

Diamond Hunter v1.1 and howlMapper v1.

### What is this repository for? ###

* A modified version of Diamond Hunter and a mapping tool for it
* DiamondHunterv1.1 & howlMapperv1
* This is written for G52SWM CW2. A coursework at The University of Nottingham

### How do I get set up? ###

Requires Java 8

This repository contains 2 eclipse projects. One of which is the game itself and the other the mapping tool.

Run the desired software via their respective jar's in the 'bin' folder.

### Tutorial ###

Key in desired coordinates for Axe and/or Boat and it will update it in game.

New game needs to be started for changes to take effect.

### References ###

Git repository on BitBucket: 
https://bitbucket.org/ImBlur96/diamond-hunter-with-howlmapper

### Authors ###
ABDUL HAZIM 015370

JONATHAN SANDIDGE 232521