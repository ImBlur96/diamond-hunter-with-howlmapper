package com.neet.DiamondHunter.Manager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


/**
 * @author Zim
 *
 */

public class Locations {

	public static int axe_X = 0;
	public static int axe_Y = 0;
	public static int boat_X = 0;
	public static int boat_Y = 0;
	
	private static String dir = "locations.map";
	
	//Reader
	private static FileReader locationsFileReader;
	private static BufferedReader locationsFileBuff;
	private static Scanner in;
	
	//Writer
	private static FileWriter locationsFileWriter;
	private static BufferedWriter locationsBuffFileWriter;
	
	public static void getLocations(){
		
		System.out.println("Initialising input stream...");
		init();
		System.out.println("Input stream established");
		
		System.out.println("Reading from file...");
		axe_X = in.nextInt();
		System.out.println("axe_X is " + axe_X);
		axe_Y = in.nextInt();
		System.out.println("axe_Y is " + axe_Y);
		boat_X = in.nextInt();
		System.out.println("boat_X is " + boat_X);
		boat_Y = in.nextInt();
		System.out.println("boat_X is " + boat_X);
		System.out.println("Successfully read locations");
		
		//close input streams
		try {
			in.close();
			locationsFileBuff.close();
			locationsFileReader.close();
		} catch (IOException e) {		
			System.err.println("Error closing input streams...");
			e.printStackTrace();
		}
		
				
	}
	
	private static void init(){
		
		try {
			locationsFileReader = new FileReader(new File(dir));
			locationsFileBuff = new BufferedReader(locationsFileReader);
			in = new Scanner(locationsFileBuff);
		} catch (FileNotFoundException e) {
			System.err.println("Error starting input stream - File Not Found");
			e.printStackTrace();
		}
		
	}
	
	private static void initWrite(){
		
		try {
			locationsFileWriter = new FileWriter(new File(dir));
			locationsBuffFileWriter = new BufferedWriter(locationsFileWriter);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void write(){
		
		System.out.println("Initialising write stream...");
		initWrite();
		System.out.println("Write stream established");
		
		//Write to file
		System.out.println("Writing to file...");
		try {
			locationsBuffFileWriter.write(axe_X + " " + axe_Y);
			System.out.println("Successfully written " + axe_X + " as new axe_X");
			System.out.println("Successfully written " + axe_Y + " as new axe_Y");
			locationsBuffFileWriter.newLine();
			locationsBuffFileWriter.write(boat_X + " " + boat_Y);
			System.out.println("Successfully written " + boat_X + " as new boat_X");
			System.out.println("Successfully written " + boat_Y + " as new boat_Y");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Close write streams
		try {
			locationsBuffFileWriter.close();
			locationsFileWriter.close();
		} catch (IOException e) {		
			System.err.println("Error closing write streams...");
			e.printStackTrace();
		}
		
	}
	
}
